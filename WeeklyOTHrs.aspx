﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="WeeklyOTHrs.aspx.cs" Inherits="WeeklyOTHrs" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">



    <script src="assets/js/master_list_jquery.min.js"></script>
    <script src="assets/js/master_list_jquery-ui.min.js"></script>
    <link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css" />
    <script>
        $(document).ready(function () {
            $('#example').dataTable();
            $('.datepicker').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true
            });
        });
    </script>


    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                    $('.select2').select2();
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });
                }
            });
        };
    </script>
    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
        <ContentTemplate>
            <!-- begin #content -->
            <div id="content" class="content">
                <!-- begin breadcrumb -->
                <ol class="breadcrumb pull-right">
                    <li><a href="javascript:;">Manual</a></li>
                    <li class="active">OT Hours</li>
                </ol>
                <!-- end breadcrumb -->
                <!-- begin page-header -->
                <h1 class="page-header">OT Hours</h1>
                <!-- end page-header -->

                <!-- begin row -->
                <div class="row">
                    <!-- begin col-12 -->
                    <div class="col-md-12">
                        <!-- begin panel -->
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                                </div>
                                <h4 class="panel-title">OT Hours</h4>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Wages Type</label>
                                                <asp:DropDownList runat="server" ID="ddlWages" class="form-control select2"
                                                    Style="width: 100%;" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlWages_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlWages" InitialValue="0" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Token No</label>
                                                <asp:TextBox runat="server" ID="txtTokenNo" class="form-control"
                                                    AutoPostBack="true" OnTextChanged="txtTokenNo_TextChanged"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Machine ID</label>

                                                <asp:DropDownList runat="server" ID="txtMachineID" class="form-control select2"
                                                    Style="width: 100%;" AutoPostBack="true"
                                                    OnSelectedIndexChanged="txtMachineID_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="txtMachineID" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->


                                    </div>
                                    <!-- end row -->
                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Employee Name</label>
                                                <asp:Label runat="server" ID="txtEmpName" class="form-control" BackColor="#F3F3F3"></asp:Label>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Department</label>
                                                <asp:Label runat="server" ID="txtDepartment" class="form-control" BackColor="#F3F3F3"></asp:Label>
                                                <asp:HiddenField ID="hdDeptId" runat="server" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Designation</label>
                                                <asp:Label runat="server" ID="txtDesignation" class="form-control" BackColor="#F3F3F3"></asp:Label>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->
                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>OT Date</label>
                                                <asp:TextBox runat="server" ID="txtOTDate" AutoComplete="off" class="form-control datepicker" placeholder="dd/MM/YYYY" AutoPostBack="true" OnTextChanged="txtOTDate_Click"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtOTDate" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ControlToValidate="txtOTDate" Display="Dynamic" ValidationGroup="Validate_FieldView" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtOTDate" ValidChars="0123456789/">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                          <div class="col-md-2">
                                            <div class="form-group">
                                                 <label>Shift</label>
                                                <asp:TextBox runat="server" ID="txtShift" AutoComplete="Off" class="form-control" AutoPostBack="true" style="text-transform:uppercase" OnTextChanged="txtOTHours_TextChanged" ></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtShift" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="UppercaseLetters,LowerCaseLetters,Numbers,Custom"
                                                    TargetControlID="txtShift" ValidChars="0123456789 NOSHIFT">
                                                </cc1:FilteredTextBoxExtender>
                                                </div>
                                             </div>
                                        
                                        <!-- end col-4 -->
                                          <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Time IN</label>
                                                <asp:TextBox runat="server" ID="txtTimeIN" AutoComplete="Off" class="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtOTHours" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtTimeIN" ValidChars="0123456789.:APM ">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Time OUT</label>
                                                <asp:TextBox runat="server" ID="txtTimeOUT" AutoComplete="Off" class="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtOTHours" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtTimeOUT" ValidChars="0123456789.:PMA ">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                         <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>OT Hours</label>
                                                <asp:TextBox runat="server" ID="txtOTHours" AutoComplete="Off" class="form-control" AutoPostBack="true" OnTextChanged="txtOTHours_TextChanged"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtOTHours" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error"
                                                    ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtOTHours" ValidChars="0123456789. ">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>One Day Pay Amount</label>
                                                <asp:TextBox runat="server" ID="txtBaseSalary" AutoComplete="Off" class="form-control" AutoPostBack="true" OnTextChanged="txtOTHours_TextChanged"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtOTHours" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtBaseSalary" ValidChars="0123456789. ">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Incentive Amount</label>
                                                <asp:TextBox runat="server" ID="txtIncentiveAmt" AutoComplete="Off" class="form-control" AutoPostBack="true" OnTextChanged="txtOTHours_TextChanged"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtIncentiveAmt" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtIncentiveAmt" ValidChars="0123456789. ">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Net Amount</label>
                                                <asp:Label runat="server" ID="lblNetAmt" class="form-control" />
                                            </div>
                                        </div>

                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->

                                        <!-- end col-4 -->
                                    </div>

                                    <div class="row" runat="server" visible="false">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <asp:CheckBox ID="chkSPGInc" runat="server" Checked="true" />
                                                Eligible For Spinning Incentive
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                    <!-- begin row -->
                                    <div class="row" runat="server" visible="false">
                                        <div class="col-md-4"></div>
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnAdd" Text="ADD" class="btn btn-success"
                                                    ValidationGroup="Validate_Field" OnClick="btnAdd_Click" />

                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-4"></div>
                                    </div>
                                    <!-- end row -->

                                    <!-- table start -->
                                    <div class="col-md-12" runat="server" visible="false">
                                        <div class="row">
                                            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                <HeaderTemplate>
                                                    <table id="example" class="display table">
                                                        <thead>
                                                            <tr>

                                                                <%--<th>Machine ID</th>--%>
                                                                <th>ExistingCode</th>
                                                                <th>Emp Name</th>
                                                                <th>OT Date</th>
                                                                <th>OT Hrs</th>
                                                                <th>Department</th>
                                                                <th>SPG.Inc</th>
                                                                <th>Mode</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <%--<td><%# Eval("MachineID")%></td>--%>
                                                        <td><%# Eval("ExistingCode")%></td>
                                                        <td><%# Eval("EmpName")%></td>
                                                        <td><%# Eval("OTDate_Str")%></td>
                                                        <td><%# Eval("OTHrs")%></td>
                                                        <td><%# Eval("Dept_Name")%></td>
                                                        <td><%# Eval("SPG_Inc")%></td>
                                                        <td>
                                                            <asp:LinkButton ID="btnEditIssueEntry_Grid" class="btn btn-primary btn-sm fa fa-pencil" runat="server"
                                                                Text="" OnCommand="GridEditEntryClick" CommandArgument='<%# Eval("OTDate_Str")+","+Eval("OTHrs")%>' CommandName='<%# Eval("MachineID")%>'>
                                                            </asp:LinkButton>

                                                            <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument='<%# Eval("OTDate_Str")+","+Eval("OTHrs")%>' CommandName='<%# Eval("MachineID")%>'
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this OT details?');">
                                                            </asp:LinkButton>
                                                        </td>

                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                    <!-- table End -->

                                    <!-- begin row -->
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnView" Text="View" class="btn btn-primary"
                                                    ValidationGroup="Validate_FieldView" OnClick="btnView_Click" />
                                                <asp:Button runat="server" ID="btnSave" Text="Save" class="btn btn-success"
                                                    OnClick="btnSave_Click" />
                                                <asp:Button runat="server" ID="btnClear" Text="Clear" class="btn btn-danger"
                                                    OnClick="btnClear_Click" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-4"></div>
                                    </div>
                                    <!-- end row -->
                                </div>
                            </div>
                        </div>
                        <!-- end panel -->
                    </div>
                    <!-- end col-12 -->
                </div>
                <!-- end row -->
            </div>
            <!-- end #content -->
        </ContentTemplate>
    </asp:UpdatePanel>



</asp:Content>

