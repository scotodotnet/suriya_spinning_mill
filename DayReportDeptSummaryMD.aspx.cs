﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;

public partial class DayReportDeptSummaryMD : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;

    string SSQL;

    string mvarUserType;
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    String CurrentYear1;
    static int CurrentYear;

    string Division;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {



            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Day Attendance Man Days Percentage Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");

                Load_Location();
                ddlunit.SelectedValue = SessionLcode;
                Drop_Shift();
            }



        }
    }
    public void Load_Location()
    {
        string SSQL = "";
        DataTable dtempty = new DataTable();
        ddlunit.DataSource = dtempty;
        ddlunit.DataBind();
        DataTable dt = new DataTable();
        SSQL = "Select LocCode as LCode from Location_Mst where CompCode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlunit.DataSource = dt;
        ddlunit.DataTextField = "LCode";
        ddlunit.DataValueField = "LCode";
        ddlunit.DataBind();


    }
    public void Drop_Shift()
    {
        string SSQL;
        DataTable dt = new DataTable();
        SSQL = "";
        SSQL = "select ShiftDesc,StartTime,EndTime from Shift_Mst Where Compcode='" + SessionCcode + "'";
        SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            ddlShift.Items.Clear();

            ddlShift.Items.Add("ALL");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ddlShift.Items.Add(dt.Rows[i]["ShiftDesc"].ToString());
            }
            // ddlShift.Items.Add("NO SHIFT");
        }

    }
    protected void btnReport_Click(object sender, EventArgs e)
    {
           
        ResponseHelper.Redirect("DayReportDeptSummaryDisplayMD.aspx?Unit=" + ddlunit.SelectedItem.Text + "&ShiftType1=" + ddlShift.SelectedValue + "&Date=" + txtFrmdate.Text, "_blank", "");

    }
}
