﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="Pay_Incentive_Mst.aspx.cs" Inherits="Pay_Incentive_Mst" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="assets/js/master_list_jquery.min.js"></script>
    <script src="assets/js/master_list_jquery-ui.min.js"></script>
    <link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').dataTable();

        });
    </script>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.select2').select2();
                    $('#example').dataTable();
                }
            });
        };
    </script>


    <!-- begin #content -->
    <div id="content" class="content">
        <ol class="breadcrumb pull-right">
            <li><a href="javascript:;">Master</a></li>
            <li class="active">Incentive Master</li>
        </ol>
        <h1 class="page-header">INCENTIVE MASTER</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                        <h4 class="panel-title">Incentive Master</h4>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">

                            <h3><u>Days Incentive</u></h3>
                            <br />
                            <div class="row">
                                <!-- begin col-4 -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Category</label>
                                        <asp:DropDownList runat="server" ID="ddlcategory" class="form-control select2"
                                            Style="width: 100%;" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlcategory_SelectedIndexChanged">
                                            <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                            <asp:ListItem Value="STAFF">STAFF</asp:ListItem>
                                            <asp:ListItem Value="LABOUR">LABOUR</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                </div>
                                <!-- end col-4 -->
                                <!-- begin col-4 -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Employee Type</label>
                                        <asp:DropDownList runat="server" ID="txtEmployeeType" class="form-control select2" Style="width: 100%;">
                                        </asp:DropDownList>

                                    </div>
                                </div>
                                <!-- end col-4 -->

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Days of Month</label>
                                        <asp:TextBox runat="server" ID="txtDaysOfMonth" class="form-control" Text="0" Style="width: 100%;"></asp:TextBox>
                                    </div>
                                </div>

                                    <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Fixed Working Days</label>
                                        <asp:TextBox runat="server" ID="txtFixedWorkDays" class="form-control" Text="0" Style="width: 100%;"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Maximum Days Worked</label>
                                        <asp:TextBox runat="server" ID="txtMinDaysWorked" class="form-control" Text="0" Style="width: 100%;"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Incentive Amount</label>
                                        <asp:TextBox runat="server" ID="txtIncent_Amount" class="form-control" Text="0.00"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row" align="center">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <br />
                                        <asp:Button runat="server" ID="btnSave" Text="Save" class="btn btn-success"
                                            OnClick="btnSave_Click" />
                                        <asp:Button runat="server" ID="btnClear" Text="Clear" class="btn btn-danger" />
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-4"></div>
                                <!-- table start -->
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                            <HeaderTemplate>
                                                <table id="example" class="display table table-responsive table-bordered table-hover">
                                                    <thead>
                                                        <tr>

                                                            <th>Emp Type</th>
                                                            <th>Month Days</th>
                                                             <th>Fixed Working Days</th>
                                                            <th>Worker Days</th>
                                                            <th>Incentive Amt</th>
                                                            <th>Mode</th>
                                                        </tr>
                                                    </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Eval("EmpType")%></td>
                                                    <td><%# Eval("MonthDays")%></td>
                                                    <td><%# Eval("WorkingDays")%></td>
                                                    <td><%# Eval("WorkerDays")%></td>
                                                    <td><%# Eval("WorkerAmt")%></td>
                                                    <td>
                                                        <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                            Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument='<%# Eval("EmpType")+","+Eval("MonthDays") %>' CommandName='<%# Eval("WorkerDays")+","+Eval("WorkingDays")%>'
                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Incentive details?');">
                                                        </asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate></table></FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                <!-- table End -->

                                <div class="col-md-4"></div>
                            </div>
                            <div class="panel-footer"></div>
                            <h3><u>Full Night Incentive</u></h3>
                            <br />
                            <div class="row">
                                <!-- begin col-4 -->
                                <div class="col-md-2">
                                    <label>Eligible Days</label>
                                    <asp:TextBox runat="server" ID="txtFUllNightEligibleDays" class="form-control" Text="0" Style="width: 100%;"></asp:TextBox>
                                </div>
                                <!-- end col-4 -->

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Amount</label>
                                        <asp:TextBox runat="server" ID="txtFUllNightAmt" class="form-control" Text="0" Style="width: 100%;"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <br />
                                        <asp:Button runat="server" ID="btnSaveFullNight" Text="Save" class="btn btn-success"
                                            OnClick="btnSaveFullNight_Click" />
                                        <asp:Button runat="server" ID="btnClearFullNight" Text="Clear" class="btn btn-danger" OnClick="btnClearFullNight_Click" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false">
                                            <HeaderTemplate>
                                                <table id="example1" class="display table table-responsive table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>Worker Days</th>
                                                            <th>Incentive Amt</th>
                                                            <th>Mode</th>
                                                        </tr>
                                                    </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Eval("Days")%></td>
                                                    <td><%# Eval("Amt")%></td>
                                                    <td>
                                                        <asp:LinkButton ID="btnDeleteEnquiry_Grid_FullNight" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                            Text="" OnCommand="btnDeleteEnquiry_Grid_FullNight_Command" CommandArgument='<%# Eval("Days")%>' CommandName='<%# Eval("Amt")%>'
                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Incentive details?');">
                                                        </asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate></table></FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

