﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class AdvancePayReport : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Query;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }

        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        if (!IsPostBack)
        {
            Load_ExistingCode();
        }
    }
    public void Load_ExistingCode()
    {
        DataTable dtdsupp = new DataTable();
        ddlWagesType.Items.Clear();
        Query = "Select EmpTypeCd,EmpType from MstEmployeeType  ";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlWagesType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpTypeCd"] = "0";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWagesType.DataTextField = "EmpType";
        ddlWagesType.DataValueField = "EmpTypeCd";
        ddlWagesType.DataBind();
    }
    protected void ddlWagesType_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtdsupp = new DataTable();
        ddlExistingCode.Items.Clear();
        Query = "Select ExistingCode from Employee_Mst where Wages='" + ddlWagesType.SelectedItem.Text + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'  ";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlExistingCode.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ExistingCode"] = "-Select-";
        dr["ExistingCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlExistingCode.DataTextField = "ExistingCode";
        ddlExistingCode.DataValueField = "ExistingCode";
        ddlExistingCode.DataBind();
    }
    protected void btnSingle_Click(object sender, EventArgs e)
    {
       string RptName ="";
        if(ddlWagesType.SelectedItem.Text =="-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select WagesType');", true);
        }

        if(ddlWagesType.SelectedItem.Text!="-Select-" && ddlExistingCode.SelectedItem.Text=="-Select-")
        {
            RptName="AllEmployeeAdvance";

            ResponseHelper.Redirect("AdvancePayReportDisplay.aspx?Wages=" + ddlWagesType.SelectedItem.Text + "&RptNam=" + RptName, "_blank", "");
      
        }
        if (ddlWagesType.SelectedItem.Text != "-Select-" && ddlExistingCode.SelectedItem.Text != "-Select-")
        {
            RptName = "SingleEmployeeAdvance";
          //  Cate=S&Depat=" + "" + "&Months=" + "" + "&yr=" + "2014" + "&fromdate=" + "" + "&ToDate=" + "" + "&Salary=" + "1" + "&CashBank=" + "1" + "&ESICode=" + "" + "&EmpType=" + "1" + "&PayslipType=" + "1" + "&PFTypePost=" + "1" + "&Left_Emp=" + "1" + "&Leftdate=" + "" + "&
            ResponseHelper.Redirect("AdvancePayReportDisplay.aspx?Wages=" + ddlWagesType.SelectedItem.Text + "&RptNam=" + RptName + "&ExistingCode=" + ddlExistingCode.SelectedItem.Text, "_blank", "");

        }

        
    }
}
