﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using Org.BouncyCastle.Utilities;
using System.IO;

public partial class Finger_Download_Upload : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string[] Time_Minus_Value_Check;
    public zkemkeeper.CZKEMClass axCZKEM1 = new zkemkeeper.CZKEMClass();
    private int iMachineNumber = 1;
    private bool bIsConnected = false;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string LocCode;
    bool Errflag;
    bool Berrflag;
    bool Connect = false;
    bool Check_Download_Clear_Error = false;
    DataTable mLocalDS = new DataTable();
    string SSQL = "";
    bool isPresent = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserName = Session["Usernmdisplay"].ToString();
            SessionUserID = Session["UserId"].ToString();
            SessionRights = Session["Rights"].ToString();

            con = new SqlConnection(constr);

        }

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Findger Print Download And Upload";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("DownloadClear"));
            //li.Attributes.Add("class", "droplink active open");


            Download_IPAddress();
            Upload_IPAddress();
            lblDwnCmpltd.Text = "";
            Initial_Data_Refresh_MachineID();
            Initial_Data_Refresh_Download_Finger();
        }
        Load_OLD_data();
        Load_OLD_Download_data();
    }

    public void Download_IPAddress()
    {
        DataTable dtdsupp = new DataTable();
        string query = "";

        ddlDownLoad_IPAddress.Items.Clear();
        query = "Select (IPAddress + ' | ' + IPMode) as IPAddress from IPAddress_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDownLoad_IPAddress.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["IPAddress"] = "-Select-";
        dr["IPAddress"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDownLoad_IPAddress.DataTextField = "IPAddress";
        ddlDownLoad_IPAddress.DataValueField = "IPAddress";
        ddlDownLoad_IPAddress.DataBind();
    }

    public void Upload_IPAddress()
    {
        DataTable dtdsupp = new DataTable();
        string query = "";

        ddlUpload_IPAddress.Items.Clear();
        query = "Select (IPAddress + ' | ' + IPMode) as IPAddress from IPAddress_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlUpload_IPAddress.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["IPAddress"] = "-Select-";
        dr["IPAddress"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlUpload_IPAddress.DataTextField = "IPAddress";
        ddlUpload_IPAddress.DataValueField = "IPAddress";
        ddlUpload_IPAddress.DataBind();
    }

    protected void btnMachineIDAdd_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        if (!ErrFlag)
        {

            //string ValuationType = qry_dt.Rows[0]["ValuationType"].ToString();
            // check view state is not null  
            if (ViewState["ItemTable_MachineID"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable_MachineID"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["MachineID"].ToString().ToUpper() == txtMachineID.Text.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Machine ID Already Added..');", true);
                    }
                }
                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["MachineID"] = txtMachineID.Text;
                    dr["Status"] = "";
                    dt.Rows.Add(dr);
                    ViewState["ItemTable_MachineID"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();
                    txtMachineID.Text = "";
                }
            }
            else
            {
                dr = dt.NewRow();
                dr["MachineID"] = txtMachineID.Text;
                dr["Status"] = "";
                dt.Rows.Add(dr);
                ViewState["ItemTable_MachineID"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
                txtMachineID.Text = "";
            }
        }
    }

    private void Initial_Data_Refresh_MachineID()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("MachineID", typeof(string)));
        dt.Columns.Add(new DataColumn("Status", typeof(string)));
        ViewState["ItemTable_MachineID"] = dt;

        //dt = Repeater1.DataSource;
    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable_MachineID"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    private void Load_OLD_Download_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable_Download"];
        Repeater2.DataSource = dt;
        Repeater2.DataBind();
    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {

        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        string qry = "";


        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable_MachineID"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["MachineID"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable_MachineID"] = dt;
        Load_OLD_data();
    }

    private void Initial_Data_Refresh_Download_Finger()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("MachineID", typeof(string)));
        dt.Columns.Add(new DataColumn("Name", typeof(string)));
        dt.Columns.Add(new DataColumn("FingerIndex", typeof(string)));
        dt.Columns.Add(new DataColumn("tmpData", typeof(string)));
        dt.Columns.Add(new DataColumn("Privilege", typeof(string)));
        dt.Columns.Add(new DataColumn("Password", typeof(string)));
        dt.Columns.Add(new DataColumn("Enabled", typeof(string)));
        dt.Columns.Add(new DataColumn("Flag", typeof(string)));
        dt.Columns.Add(new DataColumn("Status", typeof(bool)));
        //GVModule.DataSource = dt;
        //GVModule.DataBind();
        ViewState["ItemTable_Download"] = dt;

        //dt = Repeater1.DataSource;
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();

    }

    private void Clear_All_Field()
    {
        txtMachineID.Text = "";
        Initial_Data_Refresh_MachineID();
        Initial_Data_Refresh_Download_Finger();
        Load_OLD_data();
        Load_OLD_Download_data();
        ddlDownLoad_IPAddress.SelectedValue = "-Select-";
        ddlUpload_IPAddress.SelectedValue = "-Select-";
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag_Check = false;
        DataRow dr = null;
        string query = "";
        int idwErrorCode = 0;

        dt = (DataTable)ViewState["ItemTable_MachineID"];
        if (dt.Rows.Count == 0)
        {
            ErrFlag_Check = true;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First enter the machine id.');", true);
        }

        if (!ErrFlag_Check)
        {
            axCZKEM1.PullMode = 1;
            string[] IP_Add_Spilit = ddlDownLoad_IPAddress.Text.Split(new string[] { " | " }, StringSplitOptions.None);
            string Final_IP_Address = IP_Add_Spilit[0].ToString();
            bIsConnected = axCZKEM1.Connect_Net(Final_IP_Address, Convert.ToInt32("4370"));
            if (bIsConnected == true)
            {
                iMachineNumber = 1;//In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                axCZKEM1.RegEvent(iMachineNumber, 65535);//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
            }
            else
            {
                axCZKEM1.GetLastError(ref idwErrorCode);
                //MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Unable to connect the device..');", true);
                ErrFlag_Check = true;
            }
        }

        //Here Download Process Started
        if (!ErrFlag_Check)
        {
            Initial_Data_Refresh_Download_Finger();
            Load_OLD_Download_data();
            DataTable DT_Down = new DataTable();
            DataRow dr_Down = null;
            DT_Down.Columns.Add(new DataColumn("MachineID", typeof(string)));
            DT_Down.Columns.Add(new DataColumn("Name", typeof(string)));
            DT_Down.Columns.Add(new DataColumn("FingerIndex", typeof(string)));
            DT_Down.Columns.Add(new DataColumn("tmpData", typeof(string)));
            DT_Down.Columns.Add(new DataColumn("Privilege", typeof(string)));
            DT_Down.Columns.Add(new DataColumn("Password", typeof(string)));
            DT_Down.Columns.Add(new DataColumn("Enabled", typeof(string)));
            DT_Down.Columns.Add(new DataColumn("Flag", typeof(string)));
            DT_Down.Columns.Add(new DataColumn("Status", typeof(bool)));

            string sdwEnrollNumber = "";
            string sName = "";
            string sPassword = "";
            int iPrivilege = 0;
            bool bEnabled = false;

            int idwFingerIndex;
            string sTmpData = "";
            int iTmpLength = 0;
            int iFlag = 0;

            
            axCZKEM1.EnableDevice(iMachineNumber, false);
            //Cursor = Cursors.WaitCursor;
            int count = 0;
            int countID = 0;
            axCZKEM1.ReadAllUserID(iMachineNumber);//read all the user information to the memory
            axCZKEM1.ReadAllTemplate(iMachineNumber);//read all the users' fingerprint templates to the memory
            while (axCZKEM1.SSR_GetAllUserInfo(iMachineNumber, out sdwEnrollNumber, out sName, out sPassword, out iPrivilege, out bEnabled))//get all the users' information from the memory
            {
                count = count + 1;
                for (idwFingerIndex = 0; idwFingerIndex < 10; idwFingerIndex++)
                {
                    if (axCZKEM1.GetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, out iFlag, out sTmpData, out iTmpLength))//get the corresponding templates string and length from the memory
                    {
                        countID = countID + 1;
                        dr = DT_Down.NewRow();
                        //dr["ID"] = countID;
                        dr["MachineID"] = sdwEnrollNumber.ToString();
                        dr["Name"] = sName.ToString();
                        dr["FingerIndex"] = idwFingerIndex.ToString();
                        dr["tmpData"] = sTmpData.ToString();
                        dr["Privilege"] = iPrivilege.ToString();
                        dr["Password"] = sPassword.ToString();
                        if (bEnabled == true)
                        {
                            dr["Enabled"] = "true";
                        }
                        else
                        {
                            dr["Enabled"] = "false";
                        }
                        dr["Flag"] = iFlag.ToString();
                        dr["Status"] = false;
                        DT_Down.Rows.Add(dr);

                    }
                }

                ViewState["ItemTable_Download"] = DT_Down;
                Load_OLD_Download_data();

            }
            //lvDownload.EndUpdate();
            axCZKEM1.EnableDevice(iMachineNumber, true);
        }
        axCZKEM1.Disconnect();
        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Finger Download Completed...');", true);
        //Here Download Process End
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag_Check = false;
        DataRow dr = null;
        string query = "";
        int idwErrorCode = 0;

        dt = (DataTable)ViewState["ItemTable_MachineID"];
        if (dt.Rows.Count == 0)
        {
            ErrFlag_Check = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('First enter the machine id.');", true);
        }

        DataTable DT_Check = new DataTable();
        DT_Check = (DataTable)ViewState["ItemTable_Download"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag_Check = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Finger Print Data Download.');", true);
        }

        if (!ErrFlag_Check)
        {
            axCZKEM1.PullMode = 1;
            string[] IP_Add_Spilit_1 = ddlUpload_IPAddress.Text.Split(new string[] { " | " }, StringSplitOptions.None);
            string Final_IP_Address_1 = IP_Add_Spilit_1[0].ToString();
            bIsConnected = axCZKEM1.Connect_Net(Final_IP_Address_1, Convert.ToInt32("4370"));
            if (bIsConnected == true)
            {
                iMachineNumber = 1;//In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                axCZKEM1.RegEvent(iMachineNumber, 65535);//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
            }
            else
            {
                axCZKEM1.GetLastError(ref idwErrorCode);
                //MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Unable to connect the Upload Machine device..');", true);
                ErrFlag_Check = true;
            }
        }

        //Here Upload Process Started
        if (!ErrFlag_Check)
        {
            string sdwEnrollNumber = "";
            string sName = "";
            int idwFingerIndex = 0;
            string sTmpData = "";
            int iPrivilege = 0;
            string sPassword = "";
            int iFlag = 0;
            string sEnabled = "";
            bool bEnabled = false;

            //Cursor = Cursors.WaitCursor;
            axCZKEM1.EnableDevice(iMachineNumber, false);

            DataTable DT_Machine = new DataTable();
            DataTable DT_Download_M = new DataTable();
            DT_Machine = (DataTable)ViewState["ItemTable_MachineID"];
            DT_Download_M = (DataTable)ViewState["ItemTable_Download"];

            for (int i = 0; i < DT_Machine.Rows.Count; i++)
            {
                bool MachineID_Check_blb = false;
                for (int k = 0; k < DT_Download_M.Rows.Count; k++)
                {
                    if (DT_Machine.Rows[i]["MachineID"].ToString().ToUpper() == DT_Download_M.Rows[k]["MachineID"].ToString().ToUpper())
                    {
                        MachineID_Check_blb = true;
                        
                        //Here Upload to Bio-Metric Machine
                        string lblUserID = DT_Download_M.Rows[k]["MachineID"].ToString();
                        string lblName = DT_Download_M.Rows[k]["Name"].ToString();
                        string lblFingerIndex = DT_Download_M.Rows[k]["FingerIndex"].ToString();
                        string lbltmpData = DT_Download_M.Rows[k]["tmpData"].ToString();
                        string lblPrivilege = DT_Download_M.Rows[k]["Privilege"].ToString();
                        string lblPassword = DT_Download_M.Rows[k]["Password"].ToString();
                        string lblEnabled = DT_Download_M.Rows[k]["Enabled"].ToString();
                        string lblFlag = DT_Download_M.Rows[k]["Flag"].ToString();
                        //CheckBox chkbox_Data = (CheckBox)gvsal1.FindControl("ChkData");
                        //if (chkbox_Data.Checked==true)
                        //{
                        sdwEnrollNumber = lblUserID.ToString().Trim();
                        sName = lblName.ToString().Trim();
                        idwFingerIndex = Convert.ToInt32(lblFingerIndex.ToString().Trim());
                        sTmpData = lbltmpData.ToString().Trim();
                        iPrivilege = Convert.ToInt32(lblPrivilege.ToString().Trim());
                        sPassword = lblPassword.ToString().Trim();
                        sEnabled = lblEnabled.ToString().Trim();
                        iFlag = Convert.ToInt32(lblFlag.ToString());
                        if (sEnabled == "true")
                        {
                            bEnabled = true;
                        }
                        else
                        {
                            bEnabled = false;
                        }

                        if (axCZKEM1.SSR_SetUserInfo(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, bEnabled))//upload user information to the device
                        {
                            axCZKEM1.SetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, iFlag, sTmpData);//upload templates information to the device
                        }
                        else
                        {
                            axCZKEM1.GetLastError(ref idwErrorCode);
                            //MessageBox.Show("Operation failed,ErrorCode=" + idwErrorCode.ToString(), "Error");
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Operation failed ErrorCode= " + idwErrorCode.ToString() + "');", true);
                            //Cursor = Cursors.Default;
                            axCZKEM1.EnableDevice(iMachineNumber, true);
                            return;
                        }
                        //return;
                    }
                }

                if (MachineID_Check_blb == true)
                {
                    DT_Machine.Rows[i]["Status"] = "Completed";
                }
                else
                {
                    DT_Machine.Rows[i]["Status"] = "Not Matched";
                }
            }
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Finger Upload Completed...');", true);
            axCZKEM1.RefreshData(iMachineNumber);
            axCZKEM1.EnableDevice(iMachineNumber, true);
            axCZKEM1.Disconnect();
        }
        //Here Upload Process End
    }

    
}
