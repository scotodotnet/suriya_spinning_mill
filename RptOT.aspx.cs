﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Text;
using System.IO;

public partial class RptOT : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;

    double Final_Count;
    string SessionPayroll;
    string PerDayAmt;
    DataTable dt_Cat = new DataTable();
    double Present_Count;
    double Absent_Count;
    decimal Fixed_Work_Days;
    decimal Month_WH_Count;
    double Present_WH_Count;
    double NFH_Days_Count;
    double NFH_Days_Present_Count;
    string Calc_Amt = "0";

    //BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();
    DataTable MEmployeeDS = new DataTable();
    DataTable mDataSet = new DataTable();
    DataTable OTHrs_DS = new DataTable();

    string S3OT_Above_7 = "0";
    string S3OT_Above_4 = "0";
    string S3OT_Below_4 = "0";

    string OT_Above_Eight = "0";
    string OT_Above_Four = "0";
    string OT_Below_Four = "0";
    string OT_Total_Hours = "0";

    string OT_Hours = "0";

    string CmpName, Cmpaddress;
    string OT_Amt = "0";
    string Total_Amt = "0";
    string OT_Check = "0";

    DateTime date1;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string[] Time_Minus_Value_Check;
    string WagesType;
    double leaveCount;
    string Manul_OT = "";
    ReportDocument report = new ReportDocument();
    DataSet ds = new DataSet();
    DataTable dt = new DataTable();
    DataTable dt_OT = new DataTable();
    DataTable dt_Manula_OT = new DataTable();

    System.Web.UI.WebControls.DataGrid grid =
                           new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                //Load_DB();
                Page.Title = "Spay Module | OT LIST";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            // Division = Request.QueryString["Division"].ToString();
            SessionPayroll = Session["SessionEpay"].ToString();
            //string TempWages = Request.QueryString["Wages"].ToString();
            //WagesType = TempWages.Replace("_", "&");
            FromDate = Request.QueryString["FromDate"].ToString();

            if (SessionUserType == "2")
            {
                NonAdminGetAttdTable_Weekly_OTHOURS();
            }

            else
            {
                GetAttdTable_Weekly_OTHOURS();
            }


            // ds.Tables.Add(AutoDTable);
            //report.Load(Server.MapPath("crystal/OT_New.rpt"));
            if (AutoDTable.Rows.Count > 0)
            {
                SSQL = "";
                SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                //dt = objdata.RptEmployeeMultipleDetails(SSQL);
                dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dt_Cat.Rows.Count > 0)
                {
                    CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
                }

                grid.DataSource = AutoDTable;
                grid.DataBind();
                string attachment = "attachment;filename=OT.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\">" + CmpName + "</a>");
                Response.Write("--");
                Response.Write("<a style=\"font-weight:bold\">" + Cmpaddress + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\">OVER TIME REPORT - " + FromDate + "</a>");

                Response.Write("</td>");
                Response.Write("</tr>");
                //Response.Write("<tr Font-Bold='true' align='center'>");
                //Response.Write("<td colspan='10'>");
                //Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
                //Response.Write("&nbsp;&nbsp;&nbsp;");
                //Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
                //Response.Write("</td>");
                //Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.Write("<table border=1>");
                Response.Write("<tr>");
                Response.Write("<td colspan=7 align='center' style=\"font-weight:bold\"> TOTAL");
                Response.Write("</td> ");
                Response.Write("<td> =sum(H4:H" + Convert.ToInt32(AutoDTable.Rows.Count+3) + ")");
                Response.Write("</td>");
                Response.Write("<td> =sum(I4:I" + Convert.ToInt32(AutoDTable.Rows.Count + 3) + ")");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.End();
                Response.Clear();

                //report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                //report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName.ToString() + "'";
                //report.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
                //report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate + "'";
                //report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate + "'";
                //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                //CrystalReportViewer1.ReportSource = report;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert(No Data Found!!!)", true);
            }
        }
    }
    //public void Load_DB()
    //{
    //    //Get Database Name
    //    string query = "";
    //    DataTable dt_DB = new DataTable();
    //    query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [Ramalinga_Rights]..MstDBname";
    //    dt_DB = objdata.RptEmployeeMultipleDetails(query);
    //    if (dt_DB.Rows.Count > 0)
    //    {
    //        SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
    //        //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
    //    }
    //}
    public void GetAttdTable_Weekly_OTHOURS()
    {
        AutoDTable.Columns.Add("Sno");
        AutoDTable.Columns.Add("Code");
       // AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("Name");
        AutoDTable.Columns.Add("AccountNo");
        //AutoDTable.Columns.Add("Dept");
        AutoDTable.Columns.Add("Wages");
        AutoDTable.Columns.Add("OT_TimeIN");
        AutoDTable.Columns.Add("OT_TimeOUT");
     //   AutoDTable.Columns.Add("Shift");
        AutoDTable.Columns.Add("Total_OTHrs");
       // AutoDTable.Columns.Add("Total_Amt");
        AutoDTable.Columns.Add("Amount");

        DataRow dtRow = AutoDTable.NewRow();

        
        //SSQL = "";
        //SSQL = SSQL + "select (EmpNo) as Code,MachineID,(FirstName + '.' + LastName) as Name,AccountNo,Wages,'' as Total,'' as Amount,OT_Salary ";
        //SSQL = SSQL + " from Employee_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' and ";
        //SSQL = SSQL + " (IsActive='Yes' or CONVERT(DATETIME,DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)) and  OTEligible='1' ";
        //SSQL = SSQL + " order by cast(MachineID as int) asc ";

        //SSQL = "Select (OT.EmpNo) as Code,(EMP.FirstName + '.' + Emp.LastName) as Name,Emp.AccountNo,Emp.Wages,OT.Daysalary as Amount,";
        //SSQL = SSQL + " OT.TimeIN,OT.TimeOUT,OT.NoHrs, OT.netAmount From[Ramalinga_Epay]..OverTime OT ";

        SSQL = "Select  ROW_NUMBER() OVER(order by OT.EmpNo asc) [S.no],('&nbsp;'+OT.EmpNo) [Code],(EMP.FirstName + '.' + Emp.LastName) [Name],('&nbsp;'+Emp.AccountNo)[AccountNo],Emp.Wages[Wages],";
        SSQL = SSQL + " OT.TimeIN [OT_TimeIN] ,OT.TimeOUT[OT_TimeOUT],(OT.NoHrs) as Total_OTHrs,OT.netAmount[Amount] From [" + SessionPayroll + "]..OverTime OT ";
        SSQL = SSQL + " Inner join Employee_Mst Emp on Emp.EmpNo = OT.EmpNo Where OT.CCode='" + SessionCcode + "' And OT.LCode='" + SessionLcode + "' and ";
        SSQL = SSQL + " CONVERT(DATETIME,OT.TransDate,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103) and ";
        SSQL = SSQL + " Emp.OTEligible='1'  order by cast(OT.EmpNo as int) asc ";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {
            AutoDTable = dt;
        }
    }
    public void NonAdminGetAttdTable_Weekly_OTHOURS()
    { 
    
    }
    public string UTF8Encryption(string mvarPlanText)
    {
        string cipherText = "";
        try
        {
            string passPhrase = "Altius";
            string saltValue = "info@altius.co.in";
            string hashAlgorithm = "SHA1";
            string initVector = "@1B2c3D4e5F6g7H8";
            int passwordIterations = 2;
            int keySize = 256;
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            cipherText = Convert.ToBase64String(cipherTextBytes);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return cipherText;
    }

}
