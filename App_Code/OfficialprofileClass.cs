﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for OfficialprofileClass
/// </summary>
public class OfficialprofileClass
{
    private string _Dateofjoining;
    private string _Probationperiod;
    private string _Confirmationdate;
    private string _Pannumber;
    private string _ESICnumber;
    private string _PFnumber;
    private string _Grade;
    private string _EmployeeType;
    private string _ReportingAuthorityName;
    private string _InsurancecompanyName;
    private string _Insurancenumber;
    private string _Eligibleforovertime;
    private string _modeofpayement;
    private string _modepaycheque;
    private string _Banknamecheque;
    private string _EnterAmountforcheque;
    private string _BankACno;
    private string _Accountholdername;
    private string _LabourType;
    private string _ContractType;
    private string _Branchname;
    private string _Wages;
    private string _ContractPeriod;
    private string _FinancialPeriod;
    private string _BasicSalary;
    private string _EligibleESI;
    private string _ProfileType;
    private string _EligblePF;
    private string _bankname;
    private string _branchName;
    private string _Ccode;
    private string _Lcode;
    private string _EmpNo;
    
    private string _yr;
    private string _Months;
    private string _Days;
    private string _FixedDays;
    private string _YrDays;
    private string _PlannedDays;
    private string _BalanceDays;
    private string _Contractname;
    private string _ChkMonths;
    private string _ChkYears;
    private string _durationPeriod;
    private string _ESICode;

    private string _Machine;
    private string _ExpiryDate;
    private string _Salarythrough;
    private string _Enteramountfortransaction;
    private string _CreatedDate;
    private string _CreatedBy;
    private string _Modifiedby;
    private string _ModifiedOn;
    private string _ContractEndDate;
    private string _ContractStartDate;
    private string _status;
    private string _PF_Type;
    private string _duration;
    private string _BankACCno;

    public OfficialprofileClass()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string WagesType
    {
        get { return _Wages; }
        set { _Wages = value; }
    }
    public string ContractType
    {
        get { return _ContractType; }
        set { _ContractType = value; }
    }
    public string LabourType
    {
        get { return _LabourType; }
        set { _LabourType = value; }
    }
    public string Dateofjoining
    {
        get { return _Dateofjoining; }
        set { _Dateofjoining = value; }
    }
    public string Probationperiod
    {
        get { return _Probationperiod; }
        set { _Probationperiod = value; }
    }
    public string Confirmationdate
    {
        get { return _Confirmationdate; }
        set { _Confirmationdate = value; }
    }
    public string Pannumber
    {
        get { return _Pannumber; }
        set { _Pannumber = value; }
    }
    public string ESICnumber
    {
        get { return _ESICnumber; }
        set { _ESICnumber = value; }
    }
    public string PFnumber
    {
        get { return _PFnumber; }
        set { _PFnumber = value; }
    }
    public string Grade
    {
        get { return _Grade; }
        set { _Grade = value; }
    }
    public string ReportingAuthorityName
    {
        get { return _ReportingAuthorityName; }
        set { _ReportingAuthorityName = value; }
    }
    public string InsurancecompanyName
    {
        get { return _InsurancecompanyName; }
        set { _InsurancecompanyName = value; }
    }
    public string Insurancenumber
    {
        get { return _Insurancenumber; }
        set { _Insurancenumber = value; }
    }
    public string Eligibleforovertime
    {
        get { return _Eligibleforovertime; }
        set { _Eligibleforovertime = value; }
    }
    public string modeofpayment
    {
        get { return _modeofpayement; }
        set { _modeofpayement = value; }
    }
    public string modepaycheque
    {
        get { return _modepaycheque; }
        set { _modepaycheque = value; }
    }
    public string Banknamecheque
    {
        get { return _Banknamecheque; }
        set { _Banknamecheque = value; }
    }
    public string EnterAmountforcheque
    {
        get { return EnterAmountforcheque; }
        set { _EnterAmountforcheque = value; }
    }
    public string BankACno
    {
        get { return _BankACno; }
        set { _BankACno = value; }
    }
    public string BankName
    {
        get { return _bankname; }
        set { _bankname = value; }

    }
    public string BranchName
    {
        get { return _branchName; }
        set { _branchName = value; }
    }
    public string Accountholdername
    {
        get { return _Accountholdername; }
        set { _Accountholdername = value; }
    }
    public string Branchname
    {
        get { return _Branchname; }
        set { _Branchname = value; }
    }

    public string contractperiod
    {
        get { return _ContractPeriod; }
        set { _ContractPeriod = value; }
    }
    public string Financialperiod
    {
        get { return _FinancialPeriod; }
        set { _FinancialPeriod = value; }
    }
    public string BasicSalary
    {
        get { return _BasicSalary; }
        set { _BasicSalary = value; }
    }
    public string EligibleESI
    {
        get { return _EligibleESI; }
        set { _EligibleESI = value; }
    }
    public string ProfileType
    {
        get { return _ProfileType; }
        set { _ProfileType = value; }
    }
    public string EligblePF
    {
        get { return _EligblePF; }
        set { _EligblePF = value; }
    }
    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }
    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }
    public string EmpNo
    {
        get { return _EmpNo; }
        set { _EmpNo = value; }
    }
    public string yr
    {
        get { return _yr; }
        set { _yr = value; }
    }
    public string Months
    {
        get { return _Months; }
        set { _Months = value; }
    }
    public string Days1
    {
        get { return _Days; }
        set { _Days = value; }
    }
    public string FixedDays
    {
        get { return _FixedDays; }
        set { _FixedDays = value; }
    }
    
    public string YrDays
    {
        get { return _YrDays; }
        set { _YrDays = value; }
    }
    public string PlannedDays
    {
        get { return _PlannedDays; }
        set { _PlannedDays = value; }
    }
    public string BalanceDays
    {
        get { return _BalanceDays; }
        set { _BalanceDays = value; }
    }
    public string Contractname
    {
        get { return _Contractname; }
        set { _Contractname = value; }
    }
    public string ChkMonths
    {
        get { return _ChkMonths; }
        set { _ChkMonths = value; }
    }
    public string ChkYears
    {
        get { return _ChkYears; }
        set { _ChkYears = value; }
        
    }
    public string DurationPeriod
    {
        get { return _durationPeriod; }
        set { _durationPeriod = value; }
    }
    public string ESICode
    {
        get { return _ESICode; }
        set { _ESICode = value; }
    }

    public string Salarythrough
    {
        get { return _Salarythrough; }
        set { _Salarythrough = value; }
    }
    public string Enteramountfortransaction
    {
        get { return _Enteramountfortransaction; }
        set { _Enteramountfortransaction = value; }
    }

    public string Machine
    {
        get { return _Machine; }
        set { _Machine = value; }
    }

    public string ExpiryDate
    {
        get { return _ExpiryDate; }
        set { _ExpiryDate = value; }
    }

    public string CreatedBy
    {
        get { return _CreatedBy; }
        set { _CreatedBy = value; }
    }


    public string CreatedDate
    {
        get { return _CreatedDate; }
        set { _CreatedDate = value; }
    }
    public string ModifiedOn
    {
        get { return _ModifiedOn; }
        set { _ModifiedOn = value; }
    }

    public string Modifiedby
    {
        get { return _Modifiedby; }
        set { _Modifiedby = value; }
    }

    public string ContractStartDate
    {
        get { return _ContractStartDate; }
        set { _ContractStartDate = value; }
    }


    public string ContractEndDate
    {
        get { return _ContractEndDate; }
        set { _ContractEndDate = value; }
    }

    public string PF_Type
    {
        get { return _PF_Type; }
        set { _PF_Type = value; }
    }

    public string Status
    {
        get { return _status; }
        set { _status = value; }
    }

    public string Duration
    {
        get { return _duration; }
        set { _duration = value; }
    }

    public string BankACCno
    {
        get { return _BankACCno; }
        set { _BankACCno = value; }
    }
}
