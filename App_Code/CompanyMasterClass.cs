﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for CompanyMasterClass
/// </summary>
public class CompanyMasterClass
{
	public CompanyMasterClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    private string _Ccode;

    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }
    private string _Lcode;

    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }

    private string _CompanyName;

    public string CompanyName
    {
        get { return _CompanyName; }
        set { _CompanyName = value; }
    }

    private string _LocationName;

    public string LocationName
    {
        get { return _LocationName; }
        set { _LocationName = value; }
    }

    private string _AddressOne;

    public string AddressOne
    {
        get { return _AddressOne; }
        set { _AddressOne = value; }
    }

    private string _AddressTwo;

    public string AddressTwo
    {
        get { return _AddressTwo; }
        set { _AddressTwo = value; }
    }

    private string _PinCode;

    public string PinCode
    {
        get { return _PinCode; }
        set { _PinCode = value; }
    }

    private string _State;

    public string State
    {
        get { return _State; }
        set { _State = value; }
    }

    private string _MobileNumber;

    public string MobileNumber
    {
        get { return _MobileNumber; }
        set { _MobileNumber = value; }
    }


    private string _EMail;

    public string EMail
    {
        get { return _EMail; }
        set { _EMail = value; }
    }

    private string _EstablishmentCode;

    public string EstablishmentCode
    {
        get { return _EstablishmentCode; }
        set { _EstablishmentCode = value; }
    }


    private string _UserType;

    public string UserType
    {
        get { return _UserType; }
        set { _UserType = value; }
    }

    private string _UserName;

    public string UserName
    {
        get { return _UserName; }
        set { _UserName = value; }
    }


    private string _Password;

    public string Password
    {
        get { return _Password; }
        set { _Password = value; }
    }

    private string _UserDefI;

    public string UserDefI
    {
        get { return _UserDefI; }
        set { _UserDefI = value; }
    }

    private string _UserDefII;

    public string UserDefII
    {
        get { return _UserDefII; }
        set { _UserDefII = value; }
    }

    private string _UserDefIII;

    public string UserDefIII
    {
        get { return _UserDefIII; }
        set { _UserDefIII = value; }
    }

}
