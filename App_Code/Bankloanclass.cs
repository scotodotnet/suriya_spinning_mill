﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for Bankloan
/// </summary>
public class Bankloanclass
{
    private string _EmpNo;
    private string _Empname;
    private string _StafforLabour;
    private string _Department;
    private string _Designation;
    private string _Dateofjoining;
    private string _Yearofexperience;
    private string _Basicsalary;
    private string _Totalloanamount;
    private string _Alreadytakenloan;
    private string _Financialyear;
    private string _Fromdate;
    private string _Todate;
    private string _Deduction;
    private string _LoanID;
    private string _LoanName;
	public Bankloanclass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public string LoanID
    {
        get { return _LoanID; }
        set { _LoanID = value; }
    }
    public string LoanName
    {
        get { return _LoanName; }
        set { _LoanName = value; }

    }
    public string EmpNo
    {
        get { return _EmpNo; }
        set { _EmpNo = value; }
    }
    public string Empname
    {
        get { return _Empname; }
        set { _Empname = value; }
    }
    public string StafforLabour
    {
        get { return _StafforLabour;}
        set { _StafforLabour = value; }
    }
    public string Department
    {
        get { return _Department; }
        set { _Department = value; }
    }
    public string Designation
    {
        get { return _Designation; }
        set { _Designation = value; }
    }
    public string Dateofjoining
    {
        get { return _Dateofjoining; }
        set { _Dateofjoining = value; }
    }
    public string Yearofexperience
    {
        get { return _Yearofexperience; }
        set { _Yearofexperience = value; }
    }
    public string Basicsalary
    {
        get { return _Basicsalary; }
        set { _Basicsalary = value; }
    }
    public string Totalloanamount
    {
        get { return _Totalloanamount; }
        set { _Totalloanamount = value; }
    }
    public string Alreadytakenloan
    {
        get { return _Alreadytakenloan; }
        set { _Alreadytakenloan = value; }
    }
    public string Financialyear
    {
        get { return _Financialyear; }
        set { _Financialyear = value; }
    }
    public string Fromdate
    {
        get { return _Fromdate; }
        set { _Fromdate = value; }
    }
    public string Todate
    {
        get { return _Todate; }
        set { _Todate = value; }
    }
    public string Deduction
    {
        get { return _Deduction; }
        set { _Deduction = value; }
    }


        
}
