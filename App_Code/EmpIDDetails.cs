﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for EmpIDDetails
/// </summary>
public class EmpIDDetails
{
	public EmpIDDetails()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    private string _Ccode;

    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }

    private string _Lcode;

    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }

    private string _AdharNo;

    public string AdharNo
    {
        get { return _AdharNo; }
        set { _AdharNo = value; }
    }

    private string _VoterID;

    public string VoterID
    {
        get { return _VoterID; }
        set { _VoterID = value; }
    }

    private string _DrivingLicence;

    public string DrivingLicence
    {
        get { return _DrivingLicence; }
        set { _DrivingLicence = value; }
    }

    private string _PassportNo;

    public string PassportNo
    {
        get { return _PassportNo; }
        set { _PassportNo = value; }
    }

    private string _RationCardNo;

    public string RationCardNo
    {
        get { return _RationCardNo; }
        set { _RationCardNo = value; }
    }

    private string _panCardNo;

    public string panCardNo
    {
        get { return _panCardNo; }
        set { _panCardNo = value; }
    }

    private string _SmartCardNo;

    public string SmartCardNo
    {
        get { return _SmartCardNo; }
        set { _SmartCardNo = value; }
    }

    private string _Other;

    public string Other
    {
        get { return _Other; }
        set { _Other = value; }
    }
}
