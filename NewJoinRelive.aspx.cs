﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
public partial class NewJoinRelive : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;
    string OT_Eligible = "";
    string Wages_Type = "";
    string OT_Above_Eight = "0";
    string OT_Above_Four = "0";
    string OT_Below_Four = "0";
    string OT_Total_Hours = "0";

    string OT_Hours = "0";
    DataTable Log_DS = new DataTable();
    DataTable dt_OT = new DataTable();
    DataTable dt_OT_Eligible = new DataTable();

    BALDataAccess objdata = new BALDataAccess();


    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();
    DataTable MEmployeeDS = new DataTable();
    DataTable mDataSet = new DataTable();
    DataTable NFH_DS = new DataTable();
    DataTable NFH_Type_Ds = new DataTable();
    DataTable WH_DS = new DataTable();
    DataTable dt_Manula_OT = new DataTable();
    string Manul_OT;
    DateTime NFH_Date = new DateTime();
    DateTime DOJ_Date_Format = new DateTime();
    string qry_nfh = "";
    string SSQL = "";
    DateTime Week_Off_Date = new DateTime();
    DateTime WH_DOJ_Date_Format = new DateTime();
    Boolean Check_Week_Off_DOJ = false;
    double NFH_Present_Check = 0;
    decimal NFH_Days_Count = 0;
    decimal AEH_NFH_Days_Count = 0;
    decimal LBH_NFH_Days_Count = 0;
    decimal NFH_Days_Present_Count = 0;
    decimal WH_Count = 0;
    decimal WH_Present_Count = 0;
    decimal Present_Days_Count = 0;
    int Fixed_Work_Days = 0;
    decimal Spinning_Incentive_Days = 0;

    decimal NFH_Double_Wages_Checklist = 0;
    decimal NFH_Double_Wages_Statutory = 0;
    decimal NFH_Double_Wages_Manual = 0;
    decimal NFH_WH_Days_Mins = 0;
    decimal NFH_Single_Wages = 0;
    int Month_Mid_Total_Days_Count;
    string NFH_Type_Str = "";
    string NFH_Name_Get_Str = "";
    string NFH_Dbl_Wages_Statutory_Check = "";
    string Emp_WH_Day = "";
    string DOJ_Date_Str = "";


    string NFH_Date_P_Date = "";
    DateTime date1;
    DateTime date2;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string[] Time_Minus_Value_Check;
    string WagesType;
    string WagesValue;
    string oldWages;
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Payroll Attendance ";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            oldWages = Request.QueryString["Wages"].ToString();
            if (oldWages == "FITTER _ ELECTRICIANS")
            {
                WagesValue = oldWages.Replace('_', '&');
            }
            else
            {
                WagesValue = Request.QueryString["Wages"].ToString();
            }

            Division = Request.QueryString["Division"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["Todate"].ToString();

            mEmployeeDT.Columns.Add("Machine ID");
            mEmployeeDT.Columns.Add("Token No");
            mEmployeeDT.Columns.Add("EmpName");
            mEmployeeDT.Columns.Add("FatherName");
            mEmployeeDT.Columns.Add("DeptName");
            mEmployeeDT.Columns.Add("Designation");
            mEmployeeDT.Columns.Add("Wages");
            mEmployeeDT.Columns.Add("DOR");
            mEmployeeDT.Columns.Add("DOJ");
            mEmployeeDT.Columns.Add("Reason");
            mEmployeeDT.Columns.Add("BaseSalary");

            if (SessionUserType == "2")
            {
                //NonAdminPayrollAttn();
            }

            else
            {
                PayrollAttn();

            }
        }
    }
    public void PayrollAttn()
    {
        try
        {
            DataTable dt_Relive = new DataTable();
            //Get New Join
            SSQL = "";
            SSQL = SSQL + "select EmpNo,ExistingCode,FirstName,(LastName) as FatherName,DeptName,Designation,Wages,";
            SSQL = SSQL + "'' as DOR,convert(varchar(10),DOJ,103) as DOJ,Reason,";
            SSQL = SSQL + "case WHEN AgentName='-Select-' then '' else AgentName END AS AgentName,";
            SSQL = SSQL + "  BaseSalary from Employee_Mst where IsActive='Yes'  ";
            SSQL = SSQL + " and CONVERT(DATETIME,DOJ,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And  CONVERT(DATETIME,DOJ,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
            SSQL = SSQL + " Order by cast(ExistingCode as int) Asc";
            Log_DS = objdata.RptEmployeeMultipleDetails(SSQL);

            if (Log_DS.Rows.Count > 0)
            {
                //for (int i = 0; i < Log_DS.Rows.Count; i++)
                //{
                //    mEmployeeDT.NewRow();
                //    mEmployeeDT.Rows.Add();
                //    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Machine ID"] = Log_DS.Rows[i]["EmpNo"].ToString();
                //    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Token No"] = Log_DS.Rows[i]["ExistingCode"].ToString();
                //    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["EmpName"] = Log_DS.Rows[i]["FirstName"].ToString();
                //    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["FatherName"] = Log_DS.Rows[i]["FatherName"].ToString();
                //    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["DeptName"] = Log_DS.Rows[i]["DeptName"].ToString();
                //    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Designation"] = Log_DS.Rows[i]["Designation"].ToString();
                //    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Wages"] = Log_DS.Rows[i]["Wages"].ToString();
                //    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["DOR"] = Log_DS.Rows[i]["DOR"].ToString();
                //    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["DOJ"] = Log_DS.Rows[i]["DOJ"].ToString();
                //    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Reason"] = Log_DS.Rows[i]["Reason"].ToString();


                //}
            }
            //Get New Join
            SSQL = "";
            SSQL = SSQL + "select EmpNo,ExistingCode,FirstName,(LastName) as FatherName,DeptName,Designation,Wages,";
            SSQL = SSQL + "convert(varchar(10),DOR,103) as DOR,convert(varchar(10),DOJ,103) as DOJ,Reason,";
            SSQL = SSQL + "case WHEN AgentName='-Select-' then '' else AgentName END AS AgentName,";
            SSQL = SSQL + "BaseSalary from Employee_Mst where IsActive='No'  ";
            SSQL = SSQL + " and CONVERT(DATETIME,DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And  CONVERT(DATETIME,DOR,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
            SSQL = SSQL + " Order by cast(ExistingCode as int) Asc";
            dt_Relive = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_Relive.Rows.Count > 0)
            {
                //for (int i = 0; i < dt_Relive.Rows.Count; i++)
                //{
                //    mEmployeeDT.NewRow();
                //    mEmployeeDT.Rows.Add();
                //    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Machine ID"] = dt_Relive.Rows[i]["EmpNo"].ToString();
                //    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Token No"] = dt_Relive.Rows[i]["ExistingCode"].ToString();
                //    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["EmpName"] = dt_Relive.Rows[i]["FirstName"].ToString();
                //    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["FatherName"] = dt_Relive.Rows[i]["FatherName"].ToString();
                //    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["DeptName"] = dt_Relive.Rows[i]["DeptName"].ToString();
                //    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Designation"] = dt_Relive.Rows[i]["Designation"].ToString();
                //    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Wages"] = dt_Relive.Rows[i]["Wages"].ToString();
                //    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["DOR"] = dt_Relive.Rows[i]["DOR"].ToString();
                //    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["DOJ"] = dt_Relive.Rows[i]["DOJ"].ToString();
                //    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Reason"] = dt_Relive.Rows[i]["Reason"].ToString();
                //}
            }

            grid.DataSource = mEmployeeDT;
            grid.DataBind();
            string attachment = "attachment;filename=EmployeeWiseJoining.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);


            Response.Write("<table border='1'>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='13'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCcode + "</a>");
            Response.Write(" &nbsp;&nbsp;");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='13'>");
            Response.Write("<a style=\"font-weight:bold\">New Employee Joining</a>");
            Response.Write(" &nbsp;&nbsp;&nbsp; ");
            Response.Write("</td>");
            Response.Write("</tr>");


            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='13'>");

            if (FromDate != "" && ToDate != "")
            {
                Response.Write("<a style=\"font-weight:bold\">From:" + FromDate + "</a>");
                Response.Write(" &nbsp;-- &nbsp;");
                Response.Write("<a style=\"font-weight:bold\">To:" + ToDate + "</a>");
            }
            else
            {
                Response.Write("<a></a>");
            }

            Response.Write("</td>");
            Response.Write("</tr>");


            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">S.NO</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Machine ID</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Token No</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Name</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Father Name</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Department</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Designation</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Wages</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">DOJ</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">DOR</a>");
            Response.Write("</td>");

            

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Reason</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">AgentName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">BaseSalary</a>");
            Response.Write("</td>");



            Response.Write("</tr>");


            int Join = 1;
            int Relive = 1;

            for (int i = 0; i < Log_DS.Rows.Count; i++)
            {
                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td >");
                Response.Write("<a >" + Join + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + Log_DS.Rows[i]["EmpNo"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + Log_DS.Rows[i]["ExistingCode"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + Log_DS.Rows[i]["FirstName"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + Log_DS.Rows[i]["FatherName"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + Log_DS.Rows[i]["DeptName"].ToString() + "</a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + Log_DS.Rows[i]["Designation"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + Log_DS.Rows[i]["Wages"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + Log_DS.Rows[i]["DOJ"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + Log_DS.Rows[i]["DOR"].ToString() + "</a>");
                Response.Write("</td>");

                

                Response.Write("<td >");
                Response.Write("<a >" + Log_DS.Rows[i]["Reason"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + Log_DS.Rows[i]["AgentName"].ToString() + "</a>");
                Response.Write("</td>");
                Response.Write("<td >");
                Response.Write("<a >" + Log_DS.Rows[i]["BaseSalary"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("</tr>");

                Join++;

            }

            Response.Write("</table>");

            Response.Write("<table>");
            Response.Write("<tr>");
            Response.Write("<td>");
            
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");


            Response.Write("<table>");
            Response.Write("<tr>");
            Response.Write("<td>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");

            Response.Write("<table>");
            Response.Write("<tr>");
            Response.Write("<td>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            
            
            Response.Write("<table border='1'>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='13'>");
            Response.Write("<a style=\"font-weight:bold\"> RELIVE REPORT </a>");
            Response.Write(" &nbsp;&nbsp;&nbsp; ");
            Response.Write("</td>");
            Response.Write("</tr>");


            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='13'>");

            if (FromDate != "" && ToDate != "")
            {
                Response.Write("<a style=\"font-weight:bold\">From:" + FromDate + "</a>");
                Response.Write(" &nbsp;-- &nbsp;");
                Response.Write("<a style=\"font-weight:bold\">To:" + ToDate + "</a>");
            }
            else
            {
                Response.Write("<a></a>");
            }

            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">S.NO</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Machine ID</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Token No</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Name</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Father Name</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Department</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Designation</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Wages</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">DOJ</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">DOR</a>");
            Response.Write("</td>");

            

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">Reason</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">AgentName</a>");
            Response.Write("</td>");

            Response.Write("<td >");
            Response.Write("<a style=\"font-weight:bold\">BaseSalary</a>");
            Response.Write("</td>");


            Response.Write("</tr>");

            for (int i = 0; i < dt_Relive.Rows.Count; i++)
            {
                Response.Write("<tr Font-Bold='true' align='center'>");

                Response.Write("<td >");
                Response.Write("<a >" + Relive + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + dt_Relive.Rows[i]["EmpNo"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + dt_Relive.Rows[i]["ExistingCode"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + dt_Relive.Rows[i]["FirstName"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + dt_Relive.Rows[i]["FatherName"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + dt_Relive.Rows[i]["DeptName"].ToString() + "</a>");
                Response.Write("</td>");


                Response.Write("<td >");
                Response.Write("<a >" + dt_Relive.Rows[i]["Designation"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + dt_Relive.Rows[i]["Wages"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + dt_Relive.Rows[i]["DOJ"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + dt_Relive.Rows[i]["DOR"].ToString() + "</a>");
                Response.Write("</td>");

                

                Response.Write("<td >");
                Response.Write("<a >" + dt_Relive.Rows[i]["Reason"].ToString() + "</a>");
                Response.Write("</td>");

                Response.Write("<td >");
                Response.Write("<a >" + dt_Relive.Rows[i]["AgentName"].ToString() + "</a>");
                Response.Write("</td>");
                Response.Write("<td >");
                Response.Write("<a >" + dt_Relive.Rows[i]["BaseSalary"].ToString() + "</a>");
                Response.Write("</td>");




                Response.Write("</tr>");

                Relive++;

            }


            Response.Write("</table>");





            
            //  Response.Write(stw.ToString());



            Response.End();
            Response.Clear();

        }
        catch (Exception Ex)
        {
            
            throw;
        }
    }
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
}
