﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class DeptIncentive : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    string[] Time_Minus_Value_Check;

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Department Incentive";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Fin_Year_Add();
            Load_WagesType();
            Load_Data_EmpDet();
            Initial_Data_Referesh();
        }
        Load_OLD_data();
        //GVModule.Columns[34].Visible = false;
    }
    public void Fin_Year_Add()
    {
        int CurrentYear;
        int i;
        //Financial Year Add
        CurrentYear = DateTime.Now.Year;
        if (DateTime.Now.Month >= 1 && DateTime.Now.Month <= 3)
        {
            CurrentYear = CurrentYear - 1;
        }
        else
        {
            CurrentYear = CurrentYear;
        }

        ddlFinyear.Items.Clear();
        ddlFinyear.Items.Add("-Select-");

        for (i = 0; i < 11; i++)
        {
            ddlFinyear.Items.Add(Convert.ToString(CurrentYear) + "-" + Convert.ToString(CurrentYear + 1));
            CurrentYear = CurrentYear - 1;
        }
    }

    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWages.Items.Clear();
        query = "Select *from MstEmployeeType where (EmpType='HOSTEL' or EmpType='REGULAR')";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWages.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWages.DataTextField = "EmpType";
        ddlWages.DataValueField = "EmpType";
        ddlWages.DataBind();
    }

    private void Load_Data_EmpDet()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        ddlTokenNo.Items.Clear();
        query = "Select ExistingCode from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='Yes'";
        query = query + " And DeptName Not Like '%SPINNING'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        ddlTokenNo.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["ExistingCode"] = "-Select-";
        dr["ExistingCode"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        ddlTokenNo.DataTextField = "ExistingCode";
        ddlTokenNo.DataValueField = "ExistingCode";
        ddlTokenNo.DataBind();
    }

    public void Load_Data()
    {
        DataTable DT = new DataTable();
        DT.Columns.Add(new DataColumn("SNo", typeof(string)));
        DT.Columns.Add(new DataColumn("TokenNo", typeof(string)));
        DT.Columns.Add(new DataColumn("EmpName", typeof(string)));

        DT.Rows.Add();
        DT.NewRow();
        DT.Rows[DT.Rows.Count - 1]["SNo"] = "1";
        DT.Rows[DT.Rows.Count - 1]["TokenNo"] = "1";
        DT.Rows[DT.Rows.Count - 1]["EmpName"] = "EmpName";
       
       
       


        //GVModule.DataSource = DT;
        //GVModule.DataBind();
        //((CheckBox)GVModule.FindControl("chkD28")).Visible = true;
    }

    protected void ddlWages_SelectedIndexChanged(object sender, EventArgs e)
    {
      
        if (ddlMonth.SelectedItem.Text != "-Select-" && ddlFinyear.SelectedItem.Text != "-Select-" && ddlWages.SelectedItem.Text != "-Select-")
        {
            View_Old_Record();
            Local_Function_Call();
        }
    }

    protected void ddlTokenNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query;
        DataTable DT = new DataTable();

        query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            txtMachineID.Text = DT.Rows[0]["MachineID"].ToString();
            txtEmpName.Text = DT.Rows[0]["FirstName"].ToString();
            txtDepartment.Text = DT.Rows[0]["DeptName"].ToString();
        }
        else
        {
            ddlTokenNo.SelectedValue = "-Select-";
            txtEmpName.Text = "";
            txtMachineID.Text = "";
            txtDepartment.Text = "";
        }
    }

    protected void btnLoadAll_Click(object sender, EventArgs e)
    {
        Spinning_Department_Load();
    }
    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();

        dt.Columns.Add(new DataColumn("MachineID", typeof(string)));
        dt.Columns.Add(new DataColumn("TokenNo", typeof(string)));
        dt.Columns.Add(new DataColumn("EmpName", typeof(string)));
        dt.Columns.Add(new DataColumn("DeptName", typeof(string)));
        dt.Columns.Add(new DataColumn("Designation", typeof(string)));
        //dt.Columns.Add(new DataColumn("Dept_Name", typeof(string)));
        //dt.Columns.Add(new DataColumn("Designation", typeof(string)));
        //dt.Columns.Add(new DataColumn("Wages", typeof(string)));
        //dt.Columns.Add(new DataColumn("SPG_Inc", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();

        ViewState["ItemTable"] = dt;

        //dt = Repeater1.DataSource;
    }
    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }
    public void Spinning_Department_Load()
    {
        int i;
        DataTable Check_DS = new DataTable();
        bool Emp_Add_Or_Check = false;
        DataTable mDataSet=new DataTable();
        DataTable DT_Chk = new DataTable();
        string DeptIncMst = "1";

        SSQL = "Select *from Department_Inc_Mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT_Chk = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Chk.Rows.Count != 0)
        {
            DeptIncMst = DT_Chk.Rows[0]["DeptType"].ToString();
        }

        //'SSQL = "Select * from Employee_Mst where DeptName Like '%SPINNING%' And CompCode='" & iStr1(0) & "' and LocCode='" & iStr2(0) & "' And Wages='" & cmbWages.Text & "' And IsActive='YES' Order by ExistingCode Asc"
        SSQL = "Select '' as SNo,EM.ExistingCode as TokenNo,EM.FirstName as EmpName,EM.MachineID,EM.DeptName,EM.Designation,'' as TotalDays from Employee_Mst EM";
        SSQL = SSQL + " inner join Incentive_Mst Inc on EM.DeptCode COLLATE DATABASE_DEFAULT=Inc.DeptCode COLLATE DATABASE_DEFAULT";
        SSQL = SSQL + " And EM.Designation=Inc.Designation";
        SSQL = SSQL + " where EM.CompCode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And EM.Wages='" + ddlWages.SelectedItem.Text + "' And EM.IsActive='YES'";
        SSQL = SSQL + " And Inc.Ccode='" + SessionCcode + "' And Inc.Lcode='" + SessionLcode + "' And Inc.Wages='" + ddlWages.SelectedItem.Text + "'";
        //if (DeptIncMst == "2")
        //{
        //    SSQL = SSQL + " And DeptName = 'SPINNING'";
        //}
        SSQL = SSQL + " Order by EM.ExistingCode Asc";
        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
        ViewState["ItemTable"] = mDataSet;
        Repeater1.DataSource = mDataSet;
        Repeater1.DataBind();

        ALL_Total_Add();

        //if (mDataSet.Rows.Count != 0)
        //{

        //    //    'Check Already Add Or Not
        //    Emp_Add_Or_Check = false;
        //    for (i = 0; i < mDataSet.Rows.Count; i++)
        //    {
        //        SSQL = "Select * from SpinIncentiveDetPart Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Months='" + ddlMonth.SelectedItem.Text + "' And FinYear='" + ddlFinyear.SelectedItem.Text + "' And Wages='" + ddlWages.SelectedItem.Text + "' And TokenNo='" + mDataSet.Rows[i]["TokenNo"].ToString() + "' And MachineID='" + mDataSet.Rows[i]["MachineID"].ToString() + "'";
        //        Check_DS = objdata.RptEmployeeMultipleDetails(SSQL);
        //        if (Check_DS.Rows.Count != 0)
        //        {
        //            Emp_Add_Or_Check = true;
        //            break;
        //        }
        //    }

        //    //    If Emp_Add_Or_Check = True Then
        //    //        MsgBox("Spinning Employee Already Added...", MsgBoxStyle.Critical)
        //    //        Exit Sub
        //    //    End If
        //    //    flxGrid.Rows.Clear()
        //    if (Emp_Add_Or_Check == false)
        //    {
        //        GVModule.DataSource = mDataSet;
        //        GVModule.DataBind();
        //        foreach (GridViewRow gvsal in GVModule.Rows)
        //        {
        //            ((CheckBox)gvsal.FindControl("chkD1")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD2")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD3")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD4")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD5")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD6")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD7")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD8")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD9")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD10")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD11")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD12")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD13")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD14")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD15")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD16")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD17")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD18")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD19")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD20")).Checked = true;

        //            ((CheckBox)gvsal.FindControl("chkD21")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD22")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD23")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD24")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD25")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD26")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD27")).Checked = true;
        //            ((CheckBox)gvsal.FindControl("chkD28")).Checked = true;

        //            if (GVModule.Columns[30].Visible == true)
        //            {
        //                ((CheckBox)gvsal.FindControl("chkD29")).Checked = true;
        //            }
        //            else
        //            {
        //                ((CheckBox)gvsal.FindControl("chkD29")).Checked = false;
        //            }
        //            if (GVModule.Columns[31].Visible == true)
        //            {
        //                ((CheckBox)gvsal.FindControl("chkD30")).Checked = true;
        //            }
        //            else
        //            {
        //                ((CheckBox)gvsal.FindControl("chkD30")).Checked = false;
        //            }
        //            if (GVModule.Columns[32].Visible == true)
        //            {
        //                ((CheckBox)gvsal.FindControl("chkD31")).Checked = true;
        //            }
        //            else
        //            {
        //                ((CheckBox)gvsal.FindControl("chkD31")).Checked = false;
        //            }
                    

        //            //            .Rows(.Rows.Count - 1).Cells(34).Value = "31"
        //            //        End With
        //        }
        //    }
        //    ALL_Total_Add();
        //}
    }


    public void ALL_Total_Add()
    {
        Int32 Col_Total;
        Int32 i;
        Int32 j;
        string MonthDays_Get = "";
        Int32 Fin_Year_Get = Convert.ToInt32(Left_Val(ddlFinyear.SelectedItem.Text.ToString(), 4));
        double Feb_Fin_Year_Check = 0;
        string[] a;

        if (ddlMonth.SelectedItem.Text == "Jan" || ddlMonth.SelectedItem.Text == "Mar" || ddlMonth.SelectedItem.Text == "May" || ddlMonth.SelectedItem.Text == "Jul" || ddlMonth.SelectedItem.Text == "Aug" || ddlMonth.SelectedItem.Text == "Oct" || ddlMonth.SelectedItem.Text == "Dec")
        {
            MonthDays_Get = "31";
        }
        else if (ddlMonth.SelectedItem.Text == "Apr" || ddlMonth.SelectedItem.Text == "Jun" || ddlMonth.SelectedItem.Text == "Sep" || ddlMonth.SelectedItem.Text == "Nov")
        {
            MonthDays_Get = "30";
        }
        else if (ddlMonth.SelectedItem.Text == "Feb")
        {
            Feb_Fin_Year_Check = Fin_Year_Get + 1;
            Feb_Fin_Year_Check = (Feb_Fin_Year_Check / 4);
            a = Feb_Fin_Year_Check.ToString().Split('.');
            if (a[1] != "0")
            {
                MonthDays_Get = "28";
            }
            else
            {
                MonthDays_Get = "29";
            }
        }
        MonthDays_Get = (Convert.ToInt32(MonthDays_Get) + 2).ToString();
        //foreach (GridViewRow gvsal in GVModule.Rows)
        //{
        //    Col_Total = 0;

        //    if (((CheckBox)gvsal.FindControl("chkD1")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }

        //    if (((CheckBox)gvsal.FindControl("chkD2")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }

        //    if (((CheckBox)gvsal.FindControl("chkD3")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }

        //    if (((CheckBox)gvsal.FindControl("chkD4")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }

        //    if (((CheckBox)gvsal.FindControl("chkD5")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }

        //    if (((CheckBox)gvsal.FindControl("chkD6")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }
        //    if (((CheckBox)gvsal.FindControl("chkD7")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }
        //    if (((CheckBox)gvsal.FindControl("chkD8")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }
        //    if (((CheckBox)gvsal.FindControl("chkD9")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }
        //    if (((CheckBox)gvsal.FindControl("chkD10")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }

        //    if (((CheckBox)gvsal.FindControl("chkD11")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }

        //    if (((CheckBox)gvsal.FindControl("chkD12")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }

        //    if (((CheckBox)gvsal.FindControl("chkD13")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }

        //    if (((CheckBox)gvsal.FindControl("chkD14")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }

        //    if (((CheckBox)gvsal.FindControl("chkD15")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }

        //    if (((CheckBox)gvsal.FindControl("chkD16")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }
        //    if (((CheckBox)gvsal.FindControl("chkD17")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }
        //    if (((CheckBox)gvsal.FindControl("chkD18")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }
        //    if (((CheckBox)gvsal.FindControl("chkD19")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }
        //    if (((CheckBox)gvsal.FindControl("chkD20")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }

        //    if (((CheckBox)gvsal.FindControl("chkD21")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }

        //    if (((CheckBox)gvsal.FindControl("chkD22")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }

        //    if (((CheckBox)gvsal.FindControl("chkD23")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }

        //    if (((CheckBox)gvsal.FindControl("chkD24")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }

        //    if (((CheckBox)gvsal.FindControl("chkD25")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }

        //    if (((CheckBox)gvsal.FindControl("chkD26")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }
        //    if (((CheckBox)gvsal.FindControl("chkD27")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }
        //    if (((CheckBox)gvsal.FindControl("chkD28")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }
        //    if (((CheckBox)gvsal.FindControl("chkD29")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }
        //    if (((CheckBox)gvsal.FindControl("chkD30")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }
        //    if (((CheckBox)gvsal.FindControl("chkD31")).Checked == true)
        //    {
        //        Col_Total = Col_Total + 1;
        //    }
        //    else
        //    {
        //        Col_Total = Col_Total + 0;
        //    }

        //    ((Label)gvsal.FindControl("TotalDays")).Text = Col_Total.ToString();
        //}
    }

    protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlMonth.SelectedItem.Text != "-Select-" && ddlFinyear.SelectedItem.Text != "-Select-" && ddlWages.SelectedItem.Text != "-Select-")
        {
            View_Old_Record();
            Local_Function_Call();
        }
    }

    public void Local_Function_Call()
    {
        string MonthDays_Get = "";
        Int32 Fin_Year_Get = Convert.ToInt32(Left_Val(ddlFinyear.SelectedItem.Text.ToString(), 4));
        double Feb_Fin_Year_Check = 0;
        string[] a;
       if(ddlMonth.SelectedItem.Text!="-Select-" && ddlFinyear.SelectedItem.Text!="-Select-")
       {
            if(ddlMonth.SelectedItem.Text == "Jan" || ddlMonth.SelectedItem.Text == "Mar" || ddlMonth.SelectedItem.Text == "May" || ddlMonth.SelectedItem.Text == "Jul" || ddlMonth.SelectedItem.Text == "Aug" || ddlMonth.SelectedItem.Text == "Oct" || ddlMonth.SelectedItem.Text == "Dec")
            {
                MonthDays_Get = "31";
            }
            else if (ddlMonth.SelectedItem.Text == "Apr" || ddlMonth.SelectedItem.Text == "Jun" || ddlMonth.SelectedItem.Text == "Sep" || ddlMonth.SelectedItem.Text == "Nov")
            {
                MonthDays_Get = "30";
            }
            else if (ddlMonth.SelectedItem.Text == "Feb")
            {
                Feb_Fin_Year_Check = Fin_Year_Get + 1;
                Feb_Fin_Year_Check = (Feb_Fin_Year_Check / 4);
                a = Feb_Fin_Year_Check.ToString().Split('.');
                if(a[1] != "0")
                {
                    MonthDays_Get = "28";
                }
                else
                {
                    MonthDays_Get = "29";
                }
            }


            //if (MonthDays_Get == "31")
            //{
            //    GVModule.Columns[30].Visible = true;
            //    GVModule.Columns[31].Visible = true;
            //    GVModule.Columns[32].Visible = true;
            //}
            //else if (MonthDays_Get == "30")
            //{
            //    GVModule.Columns[30].Visible = true;
            //    GVModule.Columns[31].Visible = true;
            //    GVModule.Columns[32].Visible = false;
            //}
            //else if (MonthDays_Get == "29")
            //{
            //    GVModule.Columns[30].Visible = true;
            //    GVModule.Columns[31].Visible = false;
            //    GVModule.Columns[32].Visible = false;
            //}
            //else if (MonthDays_Get == "28")
            //{
            //    GVModule.Columns[30].Visible = false;
            //    GVModule.Columns[31].Visible = false;
            //    GVModule.Columns[32].Visible = false;
            //}
       }
    }

    public string Left_Val(string Value, int Length)
    {

        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }

    public string Right_Val(string Value, int Length)
    {

        int i = 0;
        i = 0;
        if (Value.Length >= Length)
        {
            //i = Value.Length - Length
            return Value.Substring(Value.Length - Length, Length);
        }
        else
        {
            return Value;
        }
    }

    protected void ddlFinyear_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlMonth.SelectedItem.Text != "-Select-" && ddlFinyear.SelectedItem.Text != "-Select-" && ddlWages.SelectedItem.Text != "-Select-")
        {
            View_Old_Record();
            Local_Function_Call();
        }
    }

   
      public void View_Old_Record()
      {

       Int32 i;
       DataTable mDataSet=new DataTable();
       SSQL = "Select SP.TokenNo,SP.MachineID,SP.EmpName,EM.DeptName,EM.Designation from SpinIncentiveDetPart SP inner join Employee_Mst EM on SP.TokenNo COLLATE DATABASE_DEFAULT=EM.ExistingCode COLLATE DATABASE_DEFAULT ";
       SSQL = SSQL + " And SP.MachineID = EM.MachineID ";
       SSQL = SSQL + " where SP.CompCode='" + SessionCcode + "' And SP.LocCode='" + SessionLcode + "' And SP.Months='" + ddlMonth.SelectedItem.Text + "' And SP.FinYear='" + ddlFinyear.SelectedItem.Text + "' And SP.Wages='" + ddlWages.SelectedItem.Text + "'";
       SSQL = SSQL + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";   
       SSQL = SSQL + " Order by SP.TokenNo Asc";
       mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
       ViewState["ItemTable"] = mDataSet;
       Repeater1.DataSource = mDataSet;
       Repeater1.DataBind();

       //if(mDataSet.Rows.Count!=0)
       //{
       //    GVModule.DataSource = mDataSet;
       //    GVModule.DataBind();
       //     for(i = 0;i< mDataSet.Rows.Count;i++)
       //     {
       //         foreach (GridViewRow gvsal in GVModule.Rows)
       //         {
       //             if(mDataSet.Rows[i]["TokenNo"].ToString()==((Label)gvsal.FindControl("TokenNo")).Text)
       //             {
       //                 if(mDataSet.Rows[i]["D1"].ToString()=="True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD1")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD1")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D2"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD2")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD2")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D3"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD3")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD3")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D4"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD4")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD4")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D5"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD5")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD5")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D6"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD6")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD6")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D7"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD7")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD7")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D8"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD8")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD8")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D9"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD9")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD9")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D10"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD10")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD10")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D11"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD11")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD11")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D12"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD12")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD12")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D13"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD13")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD13")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D14"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD14")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD14")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D15"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD15")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD15")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D16"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD16")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD16")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D17"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD17")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD17")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D18"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD18")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD18")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D19"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD19")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD19")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D20"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD20")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD20")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D21"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD21")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD21")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D22"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD22")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD22")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D23"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD23")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD23")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D24"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD24")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD24")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D25"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD25")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD25")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D26"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD26")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD26")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D27"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD27")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD27")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D28"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD28")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD28")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D29"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD29")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD29")).Checked = false;
       //                 }

       //                 if (mDataSet.Rows[i]["D30"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD30")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD30")).Checked = false;
       //                 }
       //                 if (mDataSet.Rows[i]["D31"].ToString() == "True")
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD31")).Checked = true;
       //                 }
       //                 else
       //                 {
       //                     ((CheckBox)gvsal.FindControl("chkD31")).Checked = false;
       //                 }
       //             }
                    
                  
       //         }
       //     }
       //}
       //else
       //{
       //}
      }


     
      protected void btnClear_Click(object sender, EventArgs e)
      {
          ddlMonth.SelectedValue = "-Select-";
          ddlFinyear.SelectedValue = "-Select-";
          ddlWages.SelectedValue = "-Select-";

          ddlTokenNo.SelectedValue = "-Select-";
          txtMachineID.Text = "";
          txtEmpName.Text = "";
          txtDepartment.Text = "";
          Initial_Data_Referesh();
          Load_OLD_data();
          //GVModule.DataSource = null;
          //GVModule.DataBind();
      }
      protected void btnAdd_Click(object sender, EventArgs e)
      {
          bool ErrFlag = false;

          if (ddlTokenNo.SelectedItem.Text == "-Select-")
          {
              ErrFlag = true;
              ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the TOKEN NO.!');", true);
          }
          if (ddlFinyear.SelectedItem.Text == "-Select-")
          {
              ErrFlag = true;
              ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Financial year.!');", true);
          }
          if (ddlMonth.SelectedItem.Text == "-Select-")
          {
              ErrFlag = true;
              ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Month.!');", true);
          }


          if (!ErrFlag)
          {
              OtherDeptSingleEmployee_FullMonth_Date_Add();
          }
      }

      public void OtherDeptSingleEmployee_FullMonth_Date_Add()
      {
          int i;
          DataTable Check_DS = new DataTable();
          bool Emp_Add_Or_Check = false;
          DataTable mDataSet = new DataTable();
          DataTable DT = new DataTable();
          DataRow dr = null;
          bool ErrFlag = false;

          //get datatable from view state   
          DT = (DataTable)ViewState["ItemTable"];

          //'SSQL = "Select * from Employee_Mst where DeptName Like '%SPINNING%' And CompCode='" & iStr1(0) & "' and LocCode='" & iStr2(0) & "' And Wages='" & cmbWages.Text & "' And IsActive='YES' Order by ExistingCode Asc"
          SSQL = "Select '' as SNo,ExistingCode as TokenNo,FirstName as EmpName,MachineID,DeptName,Designation,'' as TotalDays from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'  And IsActive='YES'";
          if (ddlWages.SelectedItem.Text!="-Select-")
          {
              SSQL=SSQL+"And Wages='" + ddlWages.SelectedItem.Text + "'";
          }
          if (ddlTokenNo.SelectedItem.Text != "-Select-")
          {
              SSQL = SSQL + " And ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'";
          }
          SSQL = SSQL + " Order by ExistingCode Asc";
          mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

          if (ViewState["ItemTable"] != null)
          {
              //get datatable from view state   
              DT = (DataTable)ViewState["ItemTable"];
              for (int k = 0; k < mDataSet.Rows.Count; k++)
              {
                  //check Item Already add or not
                  for (int j = 0; j < DT.Rows.Count; j++)
                  {
                      if (DT.Rows[j]["MachineID"].ToString() == mDataSet.Rows[k]["MachineID"].ToString())
                      {
                          ErrFlag = true;
                          ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Employee Already Added..');", true);
                      }
                  }
                  if (!ErrFlag)
                  {
                      dr = DT.NewRow();
                      dr["MachineID"] = mDataSet.Rows[k]["MachineID"].ToString();
                      dr["TokenNo"] = mDataSet.Rows[k]["TokenNo"].ToString();
                      dr["EmpName"] = mDataSet.Rows[k]["EmpName"].ToString();
                      dr["DeptName"] = mDataSet.Rows[k]["DeptName"].ToString();
                      dr["Designation"] = mDataSet.Rows[k]["Designation"].ToString();

                      DT.Rows.Add(dr);
                      ViewState["ItemTable"] = DT;
                      Repeater1.DataSource = DT;
                      Repeater1.DataBind();

                      ddlTokenNo.SelectedValue = "-Select-";
                      txtMachineID.Text = "";
                      txtEmpName.Text = "";
                      txtDepartment.Text = "";
                  }
              }
          }
          else
          {
              for (int k = 0; k < mDataSet.Rows.Count; k++)
              {
                  dr = DT.NewRow();
                  dr["MachineID"] = mDataSet.Rows[k]["MachineID"].ToString();
                  dr["TokenNo"] = mDataSet.Rows[k]["TokenNo"].ToString();
                  dr["EmpName"] = mDataSet.Rows[k]["EmpName"].ToString();
                  dr["DeptName"] = mDataSet.Rows[k]["DeptName"].ToString();
                  dr["Designation"] = mDataSet.Rows[k]["Designation"].ToString();

                  DT.Rows.Add(dr);
                  ViewState["ItemTable"] = DT;
                  Repeater1.DataSource = DT;
                  Repeater1.DataBind();
              }
          }
          //if (mDataSet.Rows.Count != 0)
          //{

          //    //    'Check Already Add Or Not
          //    Emp_Add_Or_Check = false;
          //    for (i = 0; i < mDataSet.Rows.Count; i++)
          //    {
          //        SSQL = "Select * from SpinIncentiveDetPart Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Months='" + ddlMonth.SelectedItem.Text + "' And FinYear='" + ddlFinyear.SelectedItem.Text + "' And Wages='" + ddlWages.SelectedItem.Text + "' And TokenNo='" + mDataSet.Rows[i]["TokenNo"].ToString() + "' And MachineID='" + mDataSet.Rows[i]["MachineID"].ToString() + "'";
          //        Check_DS = objdata.RptEmployeeMultipleDetails(SSQL);
          //        if (Check_DS.Rows.Count != 0)
          //        {
          //            Emp_Add_Or_Check = true;
          //            break;
          //        }
          //    }

          //    //    If Emp_Add_Or_Check = True Then
          //    //        MsgBox("Spinning Employee Already Added...", MsgBoxStyle.Critical)
          //    //        Exit Sub
          //    //    End If
          //    //    flxGrid.Rows.Clear()
          //    if (Emp_Add_Or_Check == false)
          //    {
          //        GVModule.DataSource = mDataSet;
          //        GVModule.DataBind();
          //        foreach (GridViewRow gvsal in GVModule.Rows)
          //        {
          //            ((CheckBox)gvsal.FindControl("chkD1")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD2")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD3")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD4")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD5")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD6")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD7")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD8")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD9")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD10")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD11")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD12")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD13")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD14")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD15")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD16")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD17")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD18")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD19")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD20")).Checked = true;

          //            ((CheckBox)gvsal.FindControl("chkD21")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD22")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD23")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD24")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD25")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD26")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD27")).Checked = true;
          //            ((CheckBox)gvsal.FindControl("chkD28")).Checked = true;

          //            if (GVModule.Columns[30].Visible == true)
          //            {
          //                ((CheckBox)gvsal.FindControl("chkD29")).Checked = true;
          //            }
          //            else
          //            {
          //                ((CheckBox)gvsal.FindControl("chkD29")).Checked = false;
          //            }
          //            if (GVModule.Columns[31].Visible == true)
          //            {
          //                ((CheckBox)gvsal.FindControl("chkD30")).Checked = true;
          //            }
          //            else
          //            {
          //                ((CheckBox)gvsal.FindControl("chkD30")).Checked = false;
          //            }
          //            if (GVModule.Columns[32].Visible == true)
          //            {
          //                ((CheckBox)gvsal.FindControl("chkD31")).Checked = true;
          //            }
          //            else
          //            {
          //                ((CheckBox)gvsal.FindControl("chkD31")).Checked = false;
          //            }


          //            //            .Rows(.Rows.Count - 1).Cells(34).Value = "31"
          //            //        End With
          //        }
          //    }
          //    ALL_Total_Add();
          //}
      }
      protected void btnSave_Click(object sender, EventArgs e)
      {
          bool ErrFlag = false;
          ALL_Total_Add();
          DataTable mDataSet = new DataTable();

          DataTable DT_check = new DataTable();
          //get datatable from view state   
          DT_check = (DataTable)ViewState["ItemTable"];
          if (DT_check.Rows.Count == 0)
          {
              ErrFlag = true;
              ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add Employee Details to save Incentive.!');", true);
          }
          if (!ErrFlag)
          {
              SSQL = "Select * from SpinIncentiveDet where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Months='" + ddlMonth.SelectedItem.Text + "' And FinYear='" + ddlFinyear.SelectedItem.Text + "' And Wages='" + ddlWages.SelectedItem.Text + "'";
              mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

              if (mDataSet.Rows.Count != 0)
              {
                  SSQL = "Delete from SpinIncentiveDet where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Months='" + ddlMonth.SelectedItem.Text + "' And FinYear='" + ddlFinyear.SelectedItem.Text + "' And Wages='" + ddlWages.SelectedItem.Text + "'";
                  objdata.RptEmployeeMultipleDetails(SSQL);
                  SSQL = "Delete from SpinIncentiveDetPart where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Months='" + ddlMonth.SelectedItem.Text + "' And FinYear='" + ddlFinyear.SelectedItem.Text + "' And Wages='" + ddlWages.SelectedItem.Text + "'";
                  objdata.RptEmployeeMultipleDetails(SSQL);
              }

              //Insert Header
              SSQL = "Insert Into SpinIncentiveDet(CompCode,LocCode,Months,FinYear,Wages) ";
              SSQL = SSQL + "Values('" + SessionCcode + "','" + SessionLcode + "','" + ddlMonth.SelectedItem.Text + "','" + ddlFinyear.SelectedItem.Text + "','" + ddlWages.SelectedItem.Text + "')";
              objdata.RptEmployeeMultipleDetails(SSQL);


              string MonthDays_Get = "";
              Int32 Fin_Year_Get = Convert.ToInt32(Left_Val(ddlFinyear.SelectedItem.Text.ToString(), 4));
              double Feb_Fin_Year_Check = 0;
              string[] a;

              if (ddlMonth.SelectedItem.Text == "Jan" || ddlMonth.SelectedItem.Text == "Mar" || ddlMonth.SelectedItem.Text == "May" || ddlMonth.SelectedItem.Text == "Jul" || ddlMonth.SelectedItem.Text == "Aug" || ddlMonth.SelectedItem.Text == "Oct" || ddlMonth.SelectedItem.Text == "Dec")
              {
                  MonthDays_Get = "31";
              }
              else if (ddlMonth.SelectedItem.Text == "Apr" || ddlMonth.SelectedItem.Text == "Jun" || ddlMonth.SelectedItem.Text == "Sep" || ddlMonth.SelectedItem.Text == "Nov")
              {
                  MonthDays_Get = "30";
              }
              else if (ddlMonth.SelectedItem.Text == "Feb")
              {
                  Feb_Fin_Year_Check = Fin_Year_Get + 1;
                  Feb_Fin_Year_Check = (Feb_Fin_Year_Check / 4);
                  a = Feb_Fin_Year_Check.ToString().Split('.');
                  if (a[1] != "0")
                  {
                      MonthDays_Get = "28";
                  }
                  else
                  {
                      MonthDays_Get = "29";
                  }
              }

              DataTable DT = new DataTable();
              //get datatable from view state   
              DT = (DataTable)ViewState["ItemTable"];

              for (int i = 0; i < DT.Rows.Count; i++)
              {
                  SSQL = "Insert Into SpinIncentiveDetPart(CompCode,LocCode,Months,FinYear,Wages,TokenNo,MachineID,EmpName,D1,D2,D3,D4,D5,D6,D7,D8,D9,D10,";
                  SSQL = SSQL + "D11,D12,D13,D14,D15,D16,D17,D18,D19,D20,D21,D22,D23,D24,D25,D26,D27,D28,D29,D30,D31,TotalDays) Values('" + SessionCcode + "','" + SessionLcode + "',";
                  SSQL = SSQL + "'" + ddlMonth.SelectedItem.Text + "','" + ddlFinyear.SelectedItem.Text + "','" + ddlWages.SelectedItem.Text + "','" + DT.Rows[i]["TokenNo"].ToString() + "','" + DT.Rows[i]["MachineID"].ToString() + "',";
                  SSQL = SSQL + "'" + DT.Rows[i]["EmpName"].ToString() + "',";
                  SSQL = SSQL + "'True','True',";
                  SSQL = SSQL + "'True','True',";
                  SSQL = SSQL + "'True','True',";
                  SSQL = SSQL + "'True','True',";
                  SSQL = SSQL + "'True','True',";
                  SSQL = SSQL + "'True','True',";
                  SSQL = SSQL + "'True','True',";
                  SSQL = SSQL + "'True','True',";
                  SSQL = SSQL + "'True','True',";
                  SSQL = SSQL + "'True','True',";
                  SSQL = SSQL + "'True','True',";
                  SSQL = SSQL + "'True','True',";
                  SSQL = SSQL + "'True','True',";
                  SSQL = SSQL + "'True','True',";
                  if (MonthDays_Get == "31")
                  {
                      SSQL = SSQL + "'True',";
                      SSQL = SSQL + "'True',";
                      SSQL = SSQL + "'True',";
                  }
                  else if (MonthDays_Get == "30")
                  {
                      SSQL = SSQL + "'True',";
                      SSQL = SSQL + "'True',";
                      SSQL = SSQL + "'False',";
                  }
                  else if (MonthDays_Get == "29")
                  {
                      SSQL = SSQL + "'True',";
                      SSQL = SSQL + "'False',";
                      SSQL = SSQL + "'False',";
                  }
                  else if (MonthDays_Get == "28")
                  {
                      SSQL = SSQL + "'False',";
                      SSQL = SSQL + "'False',";
                      SSQL = SSQL + "'False',";
                  }
                  SSQL = SSQL + "'" + MonthDays_Get + "')";
                  objdata.RptEmployeeMultipleDetails(SSQL);
              }
              ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Saved Successfully.!');", true);
              //btnClear_Click(sender, e);
          }
          //foreach (GridViewRow gvsal in GVModule.Rows)
          //{


          //    SSQL = "Insert Into SpinIncentiveDetPart(CompCode,LocCode,Months,FinYear,Wages,TokenNo,MachineID,EmpName,D1,D2,D3,D4,D5,D6,D7,D8,D9,D10,";
          //    SSQL = SSQL + "D11,D12,D13,D14,D15,D16,D17,D18,D19,D20,D21,D22,D23,D24,D25,D26,D27,D28,D29,D30,D31,TotalDays) Values('" + SessionCcode + "','" + SessionLcode + "',";
          //    SSQL = SSQL + "'" + ddlMonth.SelectedItem.Text + "','" + ddlFinyear.SelectedItem.Text + "','" + ddlWages.SelectedItem.Text + "','" + ((Label)gvsal.FindControl("TokenNo")).Text + "','" + ((Label)gvsal.FindControl("MachineID")).Text + "',";
          //    SSQL = SSQL + "'" + ((Label)gvsal.FindControl("EmpName")).Text + "',";
          //    if (((CheckBox)gvsal.FindControl("chkD1")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD2")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD3")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD4")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD5")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD6")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD7")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD8")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD9")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD10")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD11")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD12")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD13")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD14")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD15")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD16")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD17")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD18")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD19")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD20")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD21")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD22")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD23")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD24")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD25")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD26")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD27")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD28")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD29")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD30")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    if (((CheckBox)gvsal.FindControl("chkD31")).Checked == true)
          //    {
          //        SSQL = SSQL + "'True',";
          //    }
          //    else
          //    {
          //        SSQL = SSQL + "'False',";
          //    }
          //    SSQL = SSQL + "'" + ((Label)gvsal.FindControl("TotalDays")).Text + "')";

          //    objdata.RptEmployeeMultipleDetails(SSQL);
          //}
      }
      protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
      {
          DataTable DT = new DataTable();
          //get datatable from view state   
          DT = (DataTable)ViewState["ItemTable"];
          for (int i = 0; i < DT.Rows.Count; i++)
          {
              if (DT.Rows[i]["MachineID"].ToString() == e.CommandArgument.ToString())
              {
                  DT.Rows.RemoveAt(i);
                  DT.AcceptChanges();
              }
          }
          ViewState["ItemTable"] = DT;
          Repeater1.DataSource = DT;
          Repeater1.DataBind();
      }
}
